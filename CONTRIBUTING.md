Authors / Maintainers
---------------------

Dominique Bagot <dbagot@yahoo.fr>


Contributors
------------

See ``git log`` for a full list of contributors.