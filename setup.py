# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path
import os.path
#

import sys



PYTHON_INSTALL_DIR = os.path.dirname(os.path.dirname(os.__file__))
os.environ['TCL_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tcl8.6')
os.environ['TK_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tk8.6')

base = None

if sys.platform=='win32':
    base = "Win32GUI"

icone = None
if sys.platform == "win32":
    icone = "icone.ico"

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='checkProjects',
    version='1.0.0',
    description='A python package for getting projects in gitlab server',
    long_description=long_description,
    long_description_content_type="text/markdown",
    license="EUCLID LPEG 2",
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Natural Language :: English',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3'
    ],
    keywords='python package template documentation testing continuous deployment',
    packages=find_packages(exclude=['docs', 'tests','data','build','htmlcov']),
    package_data = { 'projects' : ['*.yaml', '*.json', '*.ui'] },
    setup_requires=['pytest-runner', 'python-gitlab','openpyxl<2.6.0','setuptools>=38.6.0'],  # >38.6.0 needed for markdown README.md
    tests_require=['pytest', 'pytest-cov'],
    entry_points={
        'console_scripts': [
            'checkProjects=projects.checkProjects:main'
        ],
    }
)



