#  
# Copyright (C) 2012-2020 Euclid Science Ground Segment      
#    
# This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General    
# Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)    
# any later version.    
#    
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied    
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more    
# details.    
#    
# You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to    
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA    
#

"""
Get the list of gitlab projects by gitlab groups

Examples:
--------

>>> checkprojects.py --quiet
Gitlab Group = PF-VIS: count of projects : 20
Gitlab Group = PF-NIR: count of projects : 17
(...)
Outfile generated:  gitlab_data.xlsx

>>> checkprojects --show_groups
gitlab pre-defined groups: dict_keys(['PF-VIS', 'PF-NIR', ...])


>>> %run checkprojects -g PF-MER PF-VIS
Gitlab Group = PF-MER: count of projects : 15
Outfile generated:  gitlab_data.xlsx
(...)

API documentation on Gitlab : https://docs.gitlab.com/ee/api/README.html
gitlab-python package documentation: https://python-gitlab.readthedocs.io/en/stable/
"""

import argparse
import json
import os
# Avoid exception after CTRL C capture (usefull in Jupyter)
import signal
import sys

import gitlab
import requests

signal.signal(signal.SIGINT, lambda x, y: sys.exit(0))

# disable the security certificate check in Python requests
from urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

from yaml import safe_load
import logging.config
import logging.handlers

logger = logging.getLogger(__name__)

from input_output import dictionaries, excel_sheet, file_access

SCRIPT_VERSION = "0.9"

# TODO: modify access management (linux, windows,... see .cfg format)
HOME_DIR = os.getenv("HOME")
CONF_DEFAULT_FILE = os.path.join(HOME_DIR + os.sep, "sgs_tool_access.json")
#CONF_DEFAULT_FILE = filename_with_full_path("sgs_tool_access.json")

OUTPUT_DEFAULT_FILE = "gitlab_data.xlsx"
OUTPUT_DEFAULT_SHEET = "gitlab data"

LIST_OF_PFs = ['NIR', 'VIS', 'LE3', 'SPE', 'MER', 'SHE', 'PHZ', 'SIR', 'SIM', 'LE1', 'EXT']

# Gitlab pre-defined projectsgroups dictionary (key = gitlab group name, value = gitlab group Id 
# list below has been obtained by --show_all_groups script option (get_groups function below)

GITLAB_GROUPS = {'Calibration': 393, 'COMMON TOOLS': 69, 'CT-DEVWS': 419, 'DevWS': 323, 'DevWS5': 420, 'DevWS6': 508,
                 'EuclidLibs': 73, 'Likelihood': 438, 'MBSE': 344, 'MIdI': 394, 'OU-LE3-CL': 342, 'OU-NIR': 250,
                 'OU-SIM': 194, 'OU-VIS': 191, 'PAQA': 454, 'PF-CP': 502, 'PF-EXT': 37, 'PF-IST': 184,
                 'PF-IST-Likelihood': 460, 'PF-IST-Nonlinear': 489, 'PF-LE1': 536, 'PF-LE3': 42, 'PF-LE3-CL': 220,
                 'PF-LE3-ED': 222, 'PF-LE3-GC': 218, 'PF-LE3-ID': 221, 'PF-LE3-MWNG': 224, 'PF-LE3-TD': 223,
                 'PF-LE3-VMPZ': 483, 'PF-LE3-WL': 219, 'PF-MER': 36, 'PF-NIR': 34, 'PF-PHZ': 41, 'PF-SHE': 40,
                 'PF-SHE-SL': 385, 'PF-SIM': 38, 'PF-SIR': 35, 'PF-SPE': 39, 'PF-TWG': 279, 'PF-VIS': 33,
                 'Pipelines': 178, 'PO': 71, 'ProgSession2': 479, 'Projects': 425, 'SDC-CH': 53, 'SDC-DE': 54,
                 'SDC-ES': 59, 'SDC-FI': 56, 'SDC-FR': 55, 'SDC-IT': 57, 'SDC-NL': 58, 'SDC-UK': 60, 'SPV2': 333,
                 'ST-ARCH': 62, 'ST-COORD': 61, 'ST-DM': 65, 'ST-DQ': 70, 'ST-EAS': 67, 'ST-IAL': 63, 'ST-MC': 64,
                 'ST-ORCH': 68, 'ST-TEST': 402, 'ST-TOOLS': 523, 'ST-VV': 66, 'ST-WORK': 124, 'SWG': 434,
                 'SWG-CL': 435, 'ValidationExample': 404, 'WG_SARA': 461}

# The lists below have been set manually...to be updated periodically ...

GITLAB_PF_GROUPS = {'PF-EXT': 37, 'PF-LE3': 42,
                 'PF-LE3-CL': 220, 'PF-LE3-ED': 222, 'PF-LE3-GC': 218,
                 'PF-LE3-ID': 221, 'PF-LE3-MWNG': 224, 'PF-LE3-TD': 223,
                 'PF-LE3-WL': 219, 'PF-MER': 36, 'PF-NIR': 34, 'PF-PHZ': 41,
                 'PF-SHE': 40, 'PF-SHE-SL': 385, 'PF-SIM': 38, 'PF-SIR': 35,
                 'PF-SPE': 39, 'PF-TWG': 279, 'PF-VIS': 33}

GITLAB_SDC_GROUPS = {'SDC-CH': 53, 'SDC-DE': 54,
                 'SDC-ES': 59, 'SDC-FI': 56, 'SDC-FR': 55, 'SDC-IT': 57,
                 'SDC-NL': 58, 'SDC-UK': 60}

GITLAB_ST_GROUPS = {'ST-ARCH': 62, 'ST-COORD': 61, 'ST-DM': 65, 'ST-DQ': 70, 'ST-EAS': 67,
                 'ST-IAL': 63, 'ST-MC': 64, 'ST-ORCH': 68, 'ST-TEST': 402, 'ST-VV': 66}

def get_groups(gl,verbose=True):
    """
    get all the groups found in the gitlab server
    
    Parameters
    ----------
    gl: Gitlab
        GitLab server connection
    verbose: bool
        messages printed on screen if True

    Returns
    -------
    dict [str]:int
        project groups dictionary (key = gitlab group name, value = gitlab group Id 
        example: {'PF-VIS': 33, 'PF-NIR': 34 }
    
    """
    
    groups = {}
    all_groups = gl.groups.list(all_available=True,as_list=False)
    print ("{} groups found".format(all_groups.total))
    for a_group in all_groups:
        # print (item.attributes)
        groups[a_group.name] = a_group.id
        if verbose: print("Group [id = {}] = {} ".format(a_group.id,a_group.name))
    return groups


def connect_to_gitlab(config_filename):
    """
    test the connection to the gitlab server

    Parameters
    ----------
    config_filename : str
        private sonarQube configuration file
    
    Returns
    -------
    Gitlab object
        GitLab server connection (connection done and OK)
    """

    # input_word_document should be readable and output_xl_document writable
    # access permission : ensure that the token allow to run gitlab API
    # see also https://media.readthedocs.org/pdf/python-gitlab/stable/python-gitlab.pdf
    if not file_access.file_readable(config_filename):
        raise ValueError("input configuration file {} is not readable".format(config_filename))

    # TODO  see encoding character as in python 2 : open(r'config.json', 'r')
    with open(config_filename, 'r', encoding="utf-8") as f:
        config = json.load(f)

    session = requests.Session()

    myToken = config['gitlab']['private_token']
    myUrl = config['gitlab']['url']

    if 'https' in config['gitlab']:
        https = config['gitlab']['https']
        session.proxies = {'https': https}

    gl = gitlab.Gitlab(url=myUrl,
                       private_token=myToken,
                       ssl_verify=False,
                       timeout=10000,
                       api_version=4,
                       session=session)

    gl.auth()
    return gl


def get_projects_by_group(gl, group):
    """
    get all the projects belonging to a gitlab group
    retry several times the requests (connection exception management)

    Parameters
    ----------
    gl  : Gitlab
        gitlab_object (connection done and OK)
    group : 
        gitlab object group
    
    Returns
    -------
    Gitlab list of projects
        from group.projects.list(all=True)
    """
    repeat = True
    while repeat:
        try:
            repeat = False
            # projects = gl.projects.list(search=group,all=True)
            projects = group.projects.list(all=True)

        # requests.exceptions.ConnectionError
        except requests.exceptions.ChunkedEncodingError:
            print('chunked_encoding_error happened (repeat the operation...)')
            repeat = True
            continue
        except requests.exceptions.ProxyError:
            print('ProxyError happened (repeat the operation...)')
            repeat = True
            continue
        except requests.exceptions.ConnectionError:
            print('connection_error happened')
            pass
        except Exception as e:
            print('exception ocurred')
            raise e

    return projects

def get_gitlab_version(config_filename):
    """
    TODO: procedure to be implemented
    returns the version and status of Gitlab server

    Parameters
    ----------
    config_filename:str
        private sonarQube configuration file

    Returns
    -------
    str
        Gitlab server version and status
    """

    # build and run the request
    version = "To be implemented"
    revision = "To be implemented"

    return version, revision

def get_projects_in_gitlab(gitlab_object, list_groups):
    """
    returns all the projects in a list of gitlab groups

    Parameters
    ----------
    gitlab_object : gitlab_object (connection done and OK)
    list_groups : list of str
                  list of gitlab group names
    
    Returns
    -------
    dictionaries.Dictionaries
        list of projects
    
    """
    # build and run the request
    my_projects = dictionaries.Dictionaries()
    dict_project = {}

    for group_name in list_groups:

        if not GITLAB_GROUPS[group_name]:
            print("Warning: {} is not part of the authorized gitlab group names", group_name)
            continue

        group_id = GITLAB_GROUPS[group_name]
        group = gitlab_object.groups.get(group_id)
        projects = get_projects_by_group(gitlab_object, group)
        nb_projects_by_group = 0
        for project in projects:
            dict_project.clear()
            dict_project = get_project_in_gitlab(project)
            dict_project['group'] = group_name
            my_projects.body = [dict_project]
            nb_projects_by_group += 1
        print("Gitlab Group = {}: count of projects : {}".format(group_name, nb_projects_by_group))

    return my_projects


def get_project_in_gitlab(project):
    """
    returns the gitlab field of a project

    Parameters
    ----------    
    Gitlab project
        a element list from the request gl.projects.list
    
    Returns
    -------  
    dict [str]:str 
        dictionary (key = gitlab field as 'name', 'description',... )
    """

    tag_names = ""
    try:
        tags = project.tags.list(all=True)
    except:
        pass
    else:
        for tag in tags:
            # print ('tag name= ',tag.name)
            # tag_names.append(tag.name)
            tag_names += " " + str(tag.name)
            try:
                tag_release = tag.release.tag_name
            except:
                pass
            else:
                # print('**** tag release name= ', tag.release.tag_name)
                pass
    # TODO: add more fields as tag_release above,...
    project_fields = {'name': project.name,
                      'description': project.description,
                      'http_url_to_repo': project.http_url_to_repo,
                      'default_branch': project.default_branch,
                      'tag_names': tag_names}

    return project_fields


def clone_projects_in_gitlab(config_filename, projects):
    """
    git clone all the projects in a list of gitlab groups

    Parameters
    ----------
    config_filename:str
        private sonarQube configuration file
    projects : Dictionaries
               list of gitlab projects (.git project that can be cloned)

    Returns
    -------
    dictionaries.Dictionaries
        list of projects

    """
    count_project_cloned = 0

    with open(config_filename, 'r', encoding="utf-8") as f:
        config = json.load(f)
    myToken = config['gitlab']['private_token']

    for project in projects.body:
        git_repo = project['http_url_to_repo']
        git_repo2 = git_repo.replace('https://','')
        # print ("git clone {}".format(git_repo))
        command_git = 'git clone https://oauth2:' + myToken + '@' + git_repo2
        return_code = file_access.run_command(command_git,print_command_only = False)

        if return_code != 0:
            logger.error("ERROR: return code = {}".format(return_code))
        else:
            count_project_cloned += 1

    print("{} gitlab projects cloned wih success".format(count_project_cloned))

    return count_project_cloned



def write_gitlab_data(gitlab_data, quiet, file_name, section_name=OUTPUT_DEFAULT_SHEET):
    """
    Write data from API requests on the screen or in a output file

    Parameters
    ----------
    gitlab_data: dict
        data from API requests 
    quiet: bool
        quiet mode (i.e. no display on screen) if true
    file_name: str
        Output file name (default is OUTPUT_DEFAULT_SHEET)
    section_name: str
        for spreadsheet file, name of the sheet (to be written)
    """

    # copy data into a compliant container (list of dictionaries)
    if file_name:
        if file_name.endswith(".xlsx"):
            spreadsheet = excel_sheet.ExcelSheet(file_name, gitlab_data)
            spreadsheet.write_sheet(sheet_name=section_name)
            spreadsheet.save_and_close()

        if file_name.endswith(".json"):
            gitlab_data.write_json(file_name)
        print("\nOutfile generated: ", file_name)

    if not quiet:
        gitlab_data.write_screen()
        # print("Writing sonarQube data on the screen")


def construct_parser():

    parser = argparse.ArgumentParser()

    parser.add_argument("-j",
                        "--config",
                        action="store",
                        metavar="FILE",
                        default=CONF_DEFAULT_FILE,
                        help="The private gitlab configuration file, default is " + CONF_DEFAULT_FILE)

    parser.add_argument("-q",
                        "--quiet",
                        action="store_true",
                        help="no output on the screen")

    parser.add_argument("-g",
                        "--groups",
                        action="store",
                        metavar="NAME",
                        nargs="*",
                        default=GITLAB_GROUPS.keys(),
                        help="list of existing gitlab groups")

    parser.add_argument("-o",
                        "--output",
                        action="store",
                        metavar="FILE",
                        default=OUTPUT_DEFAULT_FILE,
                        help="Spreadsheet file name for output, default is " + OUTPUT_DEFAULT_FILE)

    parser.add_argument("--show_selected_groups",
                        action="store_true",
                        help="Print the gitlab pre-defined groups")

    parser.add_argument("--show_all_groups",
                        action="store_true",
                        help="Print all the gitlab groups found")

    parser.add_argument("--clone",
                        action="store_true",
                        help="Checkout ('git clone') the gitlab projects found")

    parser.add_argument("-v",
                        "--version",
                        action="store_true",
                        help="Print the version of this script")

    # TODO: to be implemented below
    #parser.add_argument("--show_gitlab_version",
    #                    action="store_true",
    #                    help="Print the Gitlab server version.")
    return parser

def main(args=None):

    """The main routine."""
    if args is None:
        args = sys.argv[1:]

    log_file = file_access.filename_with_full_path(__file__,"logging.yaml")

    try:
        with open(log_file) as handle:
            log_cfg = safe_load(handle)
    except Exception as e:
        raise ValueError('Could not load logging configuration file: {0}'.format(e))

    logging.config.dictConfig(log_cfg)
    # create (root) logger
    logger = logging.getLogger()
    # logger.setLevel(logging.DEBUG)
    logger.debug("START __main__")

    parser = construct_parser()
    args = parser.parse_args()

    if args.show_selected_groups:
        print("gitlab pre-defined ALL groups: {} ".format(' '.join(str(key) for key in GITLAB_GROUPS.keys())))
        print("\ngitlab pre-defined PF groups: {} ".format(' '.join(str(key) for key in GITLAB_PF_GROUPS.keys())))
        print("\ngitlab pre-defined SDC groups: {} ".format(' '.join(str(key) for key in GITLAB_SDC_GROUPS.keys())))
        print("\ngitlab pre-defined ST groups: {} ".format(' '.join(str(key) for key in GITLAB_ST_GROUPS.keys())))

    elif args.version:
        logger.info("version {}".format(SCRIPT_VERSION))

    #elif args.show_gitlab_version:
    #    version, revision = get_gitlab_version(args.config)
    #    logger.info("Gitlab server version: {} (Revision: {})".format(version, revision))

    elif args.show_all_groups:
        gitlab_object = connect_to_gitlab(args.config)
        groups = get_groups(gitlab_object)
        print("\nSummary: gitlab groups dictionary = {} ".format(groups))
        print("\nReminder: gitlab pre-defined groups in this tool: {} ".format(' '.join(str(key) for key in GITLAB_GROUPS.keys())))

    elif args.clone:
        gitlab_object = connect_to_gitlab(args.config)
        projects = get_projects_in_gitlab(gitlab_object, args.groups)
        projects_cloned = clone_projects_in_gitlab(args.config, projects)

    else:
        gitlab_object = connect_to_gitlab(args.config)
        projects = get_projects_in_gitlab(gitlab_object, args.groups)
        write_gitlab_data(projects, args.quiet, args.output)


if __name__ == "__main__":
    main()